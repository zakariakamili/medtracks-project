<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/register.css') }}">
    <title>Document</title>
</head>

<body>

    <header class="header-register">
        {{-- Logo --}}
    </header>

    <div class="section-form">
        <form method="POST" action="{{ route('register') }}" class="form">
            <div> <span>Dèja un compte ?</span> <a href="{{ route('login') }}" class="connexion">Connectez-vous</a>
            </div>
            <h1 class="register-title"> Créez mon Compte </h1>
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div>
                <div><label>Email</label></div>
                <input class="form-input" placeholder="Enter your email" type="email" name="email" required
                    autofocus>
            </div>
            <div>
                <div><label>Mot de passe</label></div>
                <div>
                    <input class="form-input" placeholder="Enter your password" type="password" name="password"
                        required autocomplete="current-password">
                </div>
            </div>
            <div>
                <div>
                    <input class="form-input" type="hidden" name="role" value="professional">
                </div>
            </div>
            <button type="submit" class="form-btn">Créer mon compte</button>
        </form>
    </div>



</body>

</html>
