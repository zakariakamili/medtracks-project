<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\Establishment\EstablishmentDashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(
    [
        'prefix' => 'establishment',
        'middleware' => ['auth','verified','RoleEs']
    ],function(){

 //################################ Dashboard Establishment #########################################


           Route::get('/',[EstablishmentDashboardController::class,'create'])->name('establishment');



 //################################ Dashboard Establishment #####################################################
  
  //-----------------------------------------------------------------------------------------------------------
 
 //############################# Establishment route ##########################################


            Route::get('/index',[EstablishmentDashboardController::class,'index'])->name('establishment.index');


 //############################# end Establishment route ######################################


    });



require __DIR__.'/auth.php';