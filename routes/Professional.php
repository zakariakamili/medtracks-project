<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\Professional\ProfessionalDashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(
    [
        'prefix' => 'professional',
        'middleware' => ['auth','verified','RolePs']
    ],function(){

  //################################ Dashboard Professional #########################################

     Route::get('/',[ProfessionalDashboardController::class,'create'])->name('professional');

  //##########################################################################################  
  
  //-----------------------------------------------------------------------------------------------------------
 

  //############################# Professional route ##########################################

     Route::get('/index',[ProfessionalDashboardController::class,'index'])->name('professional.index');

  //############################# end Professional route ######################################


});



require __DIR__.'/auth.php';