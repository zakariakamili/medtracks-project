<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            'email'=> 'admins@gmail.com',
            'password' => Hash::make('password'),
            'email_verified_at'=>date("Y-m-d H:i:s", strtotime('now')),
            'role'=>'admin'
        ]);
    }
}
